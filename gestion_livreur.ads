with outil_commande;
use outil_commande;

package gestion_livreur is

	Type T_secteur is (NE, NO, CE, CO, SE, SO);
	Type T_statut_liv is (disponible, en_commande, en_pause);
	Type T_cell_liv;
	Type T_pteur_liv is access T_cell_liv;
	Type T_cell_liv is record
		nom : T_mot;
		nb_com_an : integer :=0;
		mont_com_tot : integer :=0;
		suiv : T_Pteur_liv := null;
	end record;
	Type T_tab_liv is array (T_secteur, T_statut_liv) of T_pteur_liv;

	
	
	procedure affiche_liv (liv : in T_cell_liv);
	procedure affiche_liv_liste (tete : in T_pteur_liv);
	procedure affiche_tab_liv (tab : in T_tab_liv); --Affiche la table entiere
	
	procedure ajouter_liv_liste (nom : in T_mot; tete_liste : in out T_pteur_liv); -- ajoute un donné à une liste donnée (attention statut)
	procedure supprimer_liv (nomliv : in T_mot; tete_liste : in out T_pteur_liv; fait : out boolean); -- supprime un livreur donné d’une liste donnée
	procedure change_liv_liste (nomliv : in T_mot; tete_liste_part, tete_liste_va : in out T_pteur_liv; fait : out boolean); -- change un livreur de liste et change son statut
	procedure dep_livreur (tab_liv : in out T_tab_liv; nomliv : in T_mot); --désinscription definitive d'un livreur.
	procedure enregistre_livreur (tab_liv : in out T_tab_liv); --enregistre un livreur et le met dans la bonne liste. A revoir avec un getseccure.
	
	procedure min_chiffre (tete : in T_pteur_liv; nom : out T_mot); --retourne le nom du livreur avec le plus petit chiffre
	procedure affiche_meilleurs_livreurs (tab : in T_tab_liv); --affiche le ou les meilleurs livreurs (plus de chiffre d’affaire).
	
	procedure retour_pause (tab : in out T_tab_liv; fait : out boolean);
	procedure depart_pause (tab : in out T_tab_liv; fait : out boolean);
		
	procedure nouvelle_annee (tab : in T_tab_liv);
	function meme_nom_liv (t : T_tab_liv; Nom_nv_liv : T_mot) return boolean; --true si le nom n’est pas unique.
	function nb_liv_liste (tete : T_pteur_liv) return integer; -- Compte le nombre de livreurs dans la liste.
	function liv_dispo (tab : T_tab_liv; secteur : T_secteur) return boolean; -- vrai si un livreur est dispo.
	
	
end gestion_livreur;
