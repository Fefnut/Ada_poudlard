WITH Gestion_Commande, Outil_Commande, gestion_livreur;
USE Gestion_Commande, Outil_Commande, gestion_livreur;

PACKAGE ABR IS

   TYPE T_Noeud;
   TYPE T_Arbre IS ACCESS T_Noeud;
   TYPE T_Noeud IS RECORD
      Com: T_Pteur_Com;
      Fg, Fd: T_Arbre;
   END RECORD;

   FUNCTION Recherche (A: T_Arbre; Nom_C: T_Mot) RETURN T_Pteur_Com;
   PROCEDURE Adjonction (A: IN OUT T_Arbre; Cas: IN T_Pteur_Com);
   PROCEDURE Affiche_Archive_Client (A : IN T_Arbre; Nom_C : IN T_Mot);
   FUNCTION Total_Com_Client (A : T_Arbre; Nom_C : T_Mot) RETURN Integer;
   Procedure Ajout_Arbre (A : IN OUT T_Arbre; arch : T_Tab_list_com);
   Procedure affiche_arbre (A : in T_arbre);
   
   Procedure livraison_paiement (ABR : in out T_arbre; archive, prep_att_paiement : in out T_tab_list_com; Id_com : in integer; erreur : out boolean; paiement : in boolean); -- VALIDE change le statut vers réglé ou en attente de reglement selon le choix client
   procedure paiement_diff (ABR : in out T_arbre; archive, att_p : in out T_tab_list_com; Id_com : in integer; erreur : out boolean);
   procedure livraison_commande (ABR : in out T_arbre; tab_liv : in out T_tab_liv; archive, prep_att_paiement : in out T_tab_list_com; Id_com : in integer; erreur : out boolean); --enregistre livraison d’une commande avec ou sans paiement. gère pause ou disponibilité du livreur.
   PROCEDURE Annule_Commande (Abr : in out T_arbre; T_Attente : IN OUT T_Tab_Att_Com; T_Arch : IN OUT T_Tab_List_Com; secteur : IN T_Secteur);
End ABR;
