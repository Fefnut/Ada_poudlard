WITH Gestion_livreur, Gestion_Commande, Outil_Commande, ABR, ada.text_io;
USE Gestion_livreur, Gestion_Commande, Outil_Commande, ABR, ada.text_io;

PACKAGE BODY ABR IS

   FUNCTION Recherche (A: T_Arbre; Nom_C: T_Mot) RETURN T_Pteur_Com IS
      com2: T_Pteur_Com;
   BEGIN
      IF A=NULL THEN
         RETURN NULL;
      ELSIF A.Com.nom_C = Nom_C THEN
         RETURN (A.Com);
      Else
         Com2 := Recherche(A.Fg, Nom_C);
         IF Com2=NULL THEN
            RETURN (Recherche(A.Fd, Nom_C));
         ELSE RETURN(Com2);
         END IF;
      END IF;
   END Recherche;


   PROCEDURE Adjonction (A: IN OUT T_Arbre; Cas: IN T_Pteur_Com) IS
   BEGIN
      IF A=NULL THEN
         A := NEW T_Noeud'(Cas, NULL, NULL);
      ELSIF Cas.Nom_C <= A.Com.Nom_C THEN
         Adjonction(A.Fg, Cas);
      ELSE
         Adjonction(A.Fd, Cas);
      END IF;
   END Adjonction;

   PROCEDURE Affiche_Archive_Client (A : IN T_Arbre; Nom_C : IN T_Mot) IS -- Affiche les commandes archivees d'un client
   BEGIN
      IF A/=NULL THEN
         Affiche_Archive_Client(A.Fg, Nom_C);
         IF A.Com.Nom_C = Nom_C THEN
            Affiche_cellule(A.Com.all);
         END IF;
         Affiche_Archive_Client(A.Fd, Nom_C);
      END IF;
   END Affiche_Archive_Client;
   
   Procedure affiche_arbre (A : in T_arbre) is
		
	begin
		if A /= null then
			affiche_arbre(A.fg);
			affiche_text (A.com.Nom_c); new_line;
			affiche_arbre(a.fd);
		end if;
	end affiche_arbre;

   FUNCTION Total_Com_Client (A : T_Arbre; Nom_C : T_Mot) RETURN Integer IS -- Affiche le total des commandes
             BEGIN
            IF A=NULL THEN
               RETURN 0;
            ELSif A.Com.Nom_C = Nom_C THEN
               RETURN (total_com_client(A.Fg, Nom_C) + total_com_client(A.Fd, Nom_C) + A.Com.Facture);
            ELSE
               RETURN (Total_Com_Client(A.Fg, Nom_C) + Total_Com_Client(A.Fd, Nom_C));
            End if;
      END Total_Com_Client;

      PROCEDURE Ajout_Arbre (A : IN OUT T_Arbre; Arch : T_Tab_List_Com) IS
         pt2 : T_Pteur_com;
         BEGIN
            FOR Statut IN Arch'RANGE(1) loop
               FOR Secteur IN Arch'RANGE(2) LOOP
                  Pt2 := Arch(Statut, Secteur);
                  WHILE Pt2 /= NULL LOOP
                     Adjonction(A, Pt2);
                     Pt2 := Pt2.suiv;
                  END LOOP;
               END LOOP;
            END LOOP;
            End Ajout_Arbre;
            
    Procedure livraison_paiement (ABR : in out T_arbre; archive, prep_att_paiement : in out T_tab_list_com; Id_com : in integer; erreur : out boolean; paiement : in boolean) is

	begin
		if (paiement) then
			changement_statut_commande (prep_att_paiement, archive, Id_com, erreur, Preparation, Reglee);
			adjonction (ABR, cherche_com_tab (archive, Id_com));
		else
			changement_statut_commande (prep_att_paiement, prep_att_paiement, Id_com, erreur, Preparation, Attente_reglement);
		end if;
	end livraison_paiement;
            
    procedure paiement_diff (ABR : in out T_arbre; archive, att_p : in out T_tab_list_com; Id_com : in integer; erreur : out boolean) is
		
	begin
		changement_statut_commande (att_p, archive, Id_com, erreur, Attente_reglement, reglee);
		adjonction (ABR, cherche_com_tab (archive, Id_com));
	end paiement_diff;      
            
     procedure livraison_commande (ABR : in out T_arbre; tab_liv : in out T_tab_liv; archive, prep_att_paiement : in out T_tab_list_com; Id_com : in integer; erreur : out boolean) is
		choix : character;
		paiement : boolean; --true si le client paie.
		nom_liv : T_mot;
		fait : boolean;
	begin
		nom_liv := donne_nom_liv (id_com, prep_att_paiement);
		if nom_liv /= "##############################" then
			loop
				put_line ("Le client paie t-il la commande ? o/n");
				get (choix);
				skip_line;
				exit when choix = 'o' or choix='n';
				put_line ("Choisir seulement o ou n !");
			end loop;
			if choix = 'o' then
				paiement := true;
			else
				paiement := false;
			end if;
			livraison_paiement (abr, archive, prep_att_paiement, Id_com, erreur, paiement);
			Put_line ("La commande est livree");
			new_line;
			loop
				Put_line ("Le livreur veut-il prendre une petite pause bien meritee ? o/n");
				get (choix); skip_line;
				exit when choix = 'o' or choix='n';
				put_line ("Choisir seulement o ou n !");
			end loop;
			if choix = 'o' then
				for sec in T_secteur'range loop
					change_liv_liste (nom_liv, tab_liv(sec, en_commande), tab_liv(sec, en_pause), fait);
					exit when fait;
				end loop;
			else
				for sec in t_secteur'range loop
					change_liv_liste (nom_liv, tab_liv(sec, en_commande), tab_liv(sec, disponible), fait);
					exit when fait;
				end loop;
			end if;
		else
			put_line ("Attention la commande n'existe pas !!");
			erreur := false;
		end if;
	end livraison_commande;
	
	
	PROCEDURE Annule_Commande (Abr : in out T_arbre; T_Attente : IN OUT T_Tab_Att_Com; T_Arch : IN OUT T_Tab_List_Com; secteur : IN T_Secteur) IS
		Num_Com: Integer;
		erreur: boolean;
		p_com : T_pteur_com;
	BEGIN
		put_line ("Les commandes suivantes sont annulables : ");
		p_com := T_attente(secteur).tete;
		while p_com /= null loop
			put (" - "); affiche_nombre (p_com.id); new_line;
			p_com := p_com.suiv;
		end loop;
		Put ("Veuillez saisir le numero de la commande que vous souhaitez annuler: ");
		Saisie_nombre (Num_Com);
        For Secteur in T_Secteur loop
			Change_Liste_Com(Num_Com, T_Attente(Secteur).tete, T_Arch(Annulee, Secteur), erreur);
			exit when not (erreur);
		END LOOP;
		if not erreur then
			adjonction (ABR, cherche_com_tab (T_arch, Num_com));
		end if;
	End Annule_Commande;

End ABR;
