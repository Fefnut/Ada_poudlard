with ada.text_io, gestion_commande, gestion_livreur, outil_commande, sauvegarde, abr;
use ada.text_io, gestion_commande, gestion_livreur, outil_commande, sauvegarde, abr;

procedure main is

choix_u : character;
assigne_commande : boolean; --vrai si une commande est assignable à un livreur.
secteur_assigne : T_secteur; --secteur a assigner.
erreur : boolean; --vrai si il y a une erreur.
fait : boolean; --vrai si effectuee
secteur_livraison : T_secteur;

tab_liv : T_tab_liv;
nom_liv : T_mot;
nom_c : T_mot;
p_com : T_pteur_com;

attente_com : T_tab_att_com;
tab_prep_att_paiement : T_tab_list_com (Preparation..Attente_reglement, T_secteur);
tab_archive : T_tab_list_com (Reglee..Annulee, T_secteur);
Id_com : integer; --Id d'une commande.

Abr : T_arbre;



begin

Put_Line("                    _____       _                                    ");
Put_Line("                   / ____|     | |                                   ");
Put_line("                  | |  __ _ __ | |__  _   _                          ");
Put_line("                  | | |_ | '_ \| '_ \| | | |                         ");
Put_line("                  | |__| | |_) | | | | |_| |                         ");
Put_line("                   \_____| .__/|_| |_|\__, |                         ");
Put_line("                         | |           __/ |                         ");
Put_line("                         |_|          |___/                          ");
Put_line("                                                                     ");

skip_line;

	Put_line ("Bonjour utilisateur ! Que voulez vous faire ?");
loop

	--fin de loupe, avant le exit, faire les commandes automatiques.

	peut_assigner_commande (attente_com, tab_liv, assigne_commande, secteur_assigne, id_com);

	if assigne_commande then
		assigne_com (attente_com, tab_prep_att_paiement, tab_liv, secteur_assigne, Id_com, erreur);
	end if;



	Put_line ("a - Enregistrer un nouveau livreur");
	Put_line ("b - Enregistrer une nouvelle commande");
	Put_line ("c - Confirmer la livraison d'une commande");
	Put_line ("d - Payer une commande en attente de paiement");
	Put_line ("e - Enregistrer le depart d'un livreur");
	Put_line ("f - Enregistrer le depart en pause");
	Put_line ("g - Enregistrer la fin de pause");
	Put_line ("h - Afficher toutes les commandes d'un client donne");
	Put_line ("i - Afficher le nombre de commande annulees");
	Put_line ("j - Afficher le montant total des commandes d'un client");
	Put_line ("k - Afficher le nom du ou des meilleurs livreurs");
	Put_line ("l - Enregistrer les donnees");
	Put_line ("m - Charger les donnees");
	Put_line ("n - Actualiser");
	Put_line ("o - Quitter");
	Put_line ("p - Afficher les livreurs");
	Put_line ("q - Afficher les commandes");
	Put_line ("r - Annuler une commande");
	Put_line ("s - Passer la nouvelle annee");
	Put_line ("t - Afficher tout l'abr");

	get (Choix_u);
	skip_line;

	case choix_u is
		when 'a' =>
			enregistre_livreur (tab_liv);
		when 'b' =>
			Creation_Commande (attente_com);
		when 'c' =>
			choix_secteur (secteur_livraison);
			put_line ("Les commandes suivantes sont en attente de validation : ");
			p_com := tab_prep_att_paiement(Preparation, secteur_livraison);
			while p_com /= null loop
				put (" - "); affiche_nombre (p_com.id); new_line;
				p_com := p_com.suiv;
			end loop;
			p_com := tab_prep_att_paiement(Preparation, secteur_livraison);
			if p_com = null then
				new_line;
				put_line ("AUCUNE COMMANDE"); new_line;
			else
				put ("Choisissez la commande => ");
				getsecure(Id_com); skip_line;
				begin
					livraison_commande (ABR, tab_liv, tab_archive, tab_prep_att_paiement, Id_com, erreur);
				exception
					when others => put_line ("Constraint, La commande n'a pas pu etre livree pour d'obscures raisons, peut etre avez vous tape un mauvais id ...");
				end;
				if erreur then
					put_line ("La commande n'a pas pu etre livree pour d'obscures raisons, peut etre avez vous tape un mauvais id ...");
				end if;
			end if;
		when 'd' =>
			choix_secteur (secteur_livraison);
			put_line ("Les commandes suivantes sont en attente de paiement : ");
			p_com := tab_prep_att_paiement(Attente_reglement, secteur_livraison);
			while p_com /= null loop
				put (" - "); affiche_nombre (p_com.id); new_line;
				p_com := p_com.suiv;
			end loop;
			p_com := tab_prep_att_paiement(Attente_reglement, secteur_livraison);
			if p_com = null then
				new_line;
				put_line ("AUCUNE COMMANDE"); new_line;
			else
				put ("Choisissez la commande => ");
				getsecure(Id_com); skip_line;
				begin
				paiement_diff (ABR, tab_archive, tab_prep_att_paiement, Id_com, erreur);
				exception
					when others => put_line ("Constraint, La commande n'a pas pu etre payee pour d'obscures raisons, peut etre avez vous tape un mauvais id ...");
				end;
				if erreur then
					put_line ("La commande n'a pas pu etre payee pour d'obscures raisons, peut etre avez vous tape un mauvais id ...");
				end if;
			end if;
		when 'e' =>
			put ("Quel est le nom du livreur a desinscrire ?"); new_line;
			saisie_text (nom_liv);
			if meme_nom_liv (tab_liv, nom_liv) then
				dep_livreur (tab_liv, nom_liv);
			else
				put_line ("Ce livreur n'existe pas !");
			end if;
		when 'f' =>
			depart_pause (tab_liv, fait);
			if not(fait) then
				put_line ("Le livreur n'a pas pu etre change de liste, etes vous sur d'avoir insere le bon nom de livreur ?");
			end if;
		when 'g' =>
			retour_pause (tab_liv, fait);
			if not(fait) then
				put_line ("Le livreur n'a pas pu etre change de liste, etes vous sur d'avoir insere le bon nom de livreur ?");
			end if;
		when 'h' =>
			put_line ("Quel est le nom du client ? ");
			saisie_text (Nom_c);
			Affiche_Archive_Client (Abr, Nom_C);
		when 'i' =>
			Put ("Il y a actuellement "); affiche_nombre(nb_com_annule (tab_archive)); put_line (" commandes annulees.");
		when 'j' =>
			put_line ("Quel est le nom du client ? ");
			saisie_text (Nom_c);
			Affiche_nombre(Total_Com_Client (ABr, Nom_C));
			new_line;
		when 'k' =>
			affiche_meilleurs_livreurs (tab_liv);
		when 'l' =>
			sauv_tous_liv (tab_liv);
			sauv_tous_com (attente_com, tab_prep_att_paiement, tab_archive);
		when 'm' =>
			tab_liv := charge_tous_liv;
			charge_tous_com (attente_com, tab_prep_att_paiement, tab_archive);
			Ajout_Arbre (Abr, tab_archive);
		when 'n' =>
			null;
		when 'o' =>
			exit;
		when 'p' =>
			affiche_tab_liv (tab_liv);
		when 'q' =>
			affiche_toute_commande (attente_com, tab_prep_att_paiement, tab_archive);
		when 'r' =>
			choix_secteur (secteur_livraison);
			Annule_Commande (abr, attente_com, tab_archive, secteur_livraison);
		when 's' =>
			nouvelle_annee (tab_liv);
		when 't' =>
			affiche_arbre (AbR);
			new_line;
		when others =>
			null;
	end case;

end loop;

end main;
