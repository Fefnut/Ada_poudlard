with ada.text_io, ada.integer_text_io, ada.unchecked_deallocation, outil_commande;
use ada.text_io, ada.integer_text_io, outil_commande;

package body gestion_livreur is

	
	function meme_nom_liv (t : T_tab_liv; Nom_nv_liv : T_mot) return boolean is
		p : T_pteur_liv;
	begin
		for sec in t'range(1) loop
			for stat in t'range(2) loop
				p := t(sec, stat);
				while p/= null loop
					if p.nom = nom_nv_liv then
						return true;
					end if;
					p := p.suiv;
				end loop;
			end loop;
		end loop;
		return false;
	end meme_nom_liv;
	
	
	
	procedure liberer_liv is new ada.unchecked_deallocation (T_cell_liv, T_pteur_liv);
	
	procedure affiche_liv (liv : in T_cell_liv) is 
		
	begin
		put("    Nom => ") ; affiche_text( liv.nom); new_line;
		put("    Nb commande => "); affiche_nombre (liv.nb_com_an);
		new_line;
		put ("    Montant total des commande => "); affiche_nombre(liv.mont_com_tot);
		new_line;

		new_line;
	end affiche_liv;
	
	procedure affiche_liv_liste (tete : in T_pteur_liv) is
		p : T_pteur_liv := tete;
	begin
		while p /= null loop
			affiche_liv(p.all);
			p := p.suiv;
		end loop;
	end affiche_liv_liste;
	
	procedure ajouter_liv_liste (nom : in T_mot; tete_liste : in out T_pteur_liv) is
		
	begin
			tete_liste := new T_cell_liv'(nom, 0, 0, tete_liste);
	end ajouter_liv_liste;
	
	
	procedure supprimer_liv (nomliv : in T_mot; tete_liste : in out T_pteur_liv; fait : out boolean) is
		q : T_pteur_liv;
	begin
		if tete_liste = null then
			fait := false;
		elsif tete_liste.nom = nomliv then 
			q := tete_liste;
			tete_liste := tete_liste.suiv;
			liberer_liv (q);
			fait := true;
		else
			supprimer_liv (nomliv, tete_liste.suiv, fait);
		end if;
	end supprimer_liv;
	

	
	procedure change_liv_liste (nomliv : in T_mot; tete_liste_part, tete_liste_va : in out T_pteur_liv; fait : out boolean) is
		aux : T_pteur_liv;
	begin
		
		if tete_liste_part = null then
			fait := false;
		elsif tete_liste_part.nom = nomliv then
			aux := tete_liste_va;
			tete_liste_va := tete_liste_part;
			tete_liste_part := tete_liste_part.suiv;
			tete_liste_va.suiv := aux;
			fait := true;
		else
			change_liv_liste (nomliv, tete_liste_part.suiv, tete_liste_va, fait);
		end if;
	end change_liv_liste;
	
	procedure dep_livreur (tab_liv : in out T_tab_liv; nomliv : in T_mot) is
		fait : boolean := false;
	begin
		for secteur in tab_liv'range(1) loop
			for statut in tab_liv'range(2) loop
				supprimer_liv(nomliv, tab_liv(secteur, statut), fait);
				if fait then
					exit;
				end if;
			end loop;
		end loop;
	end dep_livreur;
	
	procedure enregistre_livreur (tab_liv : in out T_tab_liv) is --ATTENTION IL FAUT METTRE GETSECURE/Saisie_Text ...
		nom : T_mot;
		choix_sec, choix_sta : integer;
	begin 
		loop
		put ("Veuillez saisir le nom du livreur => ");
		Saisie_Text (nom);
		exit when not(meme_nom_liv (tab_liv,nom));
		put_line ("Nom déja utilisé veuillez entrer un nom different");
		end loop;
		Put ("Dans quel secteur inclure le livreur ? "); new_line;
		for i in T_secteur'range loop
			put(T_secteur'pos(i),1); put (" : " & T_secteur'image(i)); new_line;
		end loop;
		loop 
			begin
				put ("Secteur => ");
				get(choix_sec); skip_line;
				exit when choix_sec in T_secteur'pos(T_secteur'first)..T_secteur'pos(T_secteur'last);
				put ("Choisir des valeurs comprises entre "); put(T_secteur'pos(T_secteur'first),1); put (" et "); put(T_secteur'pos(T_secteur'last),1); new_line;
			exception
			when others =>skip_line; put_line ("Erreur de saisie, recommencer");
			end;
		end loop;
		
		Put ("Dans quel statut mettre le livreur ?"); new_line;
		for i in T_statut_liv'range loop
			put(T_statut_liv'pos(i),1); put (" : " & T_statut_liv'image(i)); new_line;
		end loop;
		loop 
			begin
				put ("Statut => ");
				get(choix_sta); skip_line;
				exit when choix_sta in T_statut_liv'pos(T_statut_liv'first)..T_statut_liv'pos(T_statut_liv'last);
				put ("Choisir des valeurs comprises entre "); put(T_statut_liv'pos(T_statut_liv'first),1); put (" et "); put(T_statut_liv'pos(T_statut_liv'last),1); new_line;
			exception
			when others =>skip_line; put_line ("Erreur de saisie, recommencer");
			end;
		end loop;
		
		ajouter_liv_liste (nom, tab_liv(T_secteur'val(choix_sec), T_statut_liv'val(choix_sta))); 
	end enregistre_livreur;
	
	procedure affiche_tab_liv (tab : in T_tab_liv) is
		
	begin
		for i in T_secteur'range loop
			put (T_secteur'image(i)); put_line (" : ");
			for j in T_statut_liv'range loop
				put_line("  " & T_statut_liv'image(j));
				affiche_liv_liste(tab(i, j));
			end loop;
		end loop;
	end affiche_tab_liv;
	

	procedure min_chiffre (tete : in T_pteur_liv; nom : out T_mot) is
		p : T_pteur_liv := tete;
		min : integer := integer'last;
	begin
		while p /= null loop
			if p.mont_com_tot < min then
				nom := p.nom;
				min := p.mont_com_tot;
			end if;
			p := p.suiv;
		end loop;
	end min_chiffre;

	procedure affiche_meilleurs_livreurs (tab : in T_tab_liv) is --affiche le ou les meilleurs livreurs (plus de chiffre d’affaire).
		
		function cherche_max (tab : T_tab_liv) return integer is
			p : T_pteur_liv;
			max : integer := -1;
		begin
			for secteur in T_secteur'range loop
				for statut in T_statut_liv'range loop
					p := tab(secteur, statut);
					while p /= null loop
						if p.mont_com_tot > max then
							max := p.mont_com_tot;
						end if;
						p := p.suiv;
					end loop;
				end loop;
			end loop;
			return max;
		end cherche_max;
		
		max : integer := cherche_max (tab);
		p : T_pteur_liv;
	begin
		for secteur in T_secteur'range loop
				for statut in T_statut_liv'range loop
					p := tab(secteur, statut);
					while p /= null loop
						if p.mont_com_tot = max then
							affiche_liv(p.all);
						end if;
						p := p.suiv;
					end loop;
				end loop;
			end loop;
	end affiche_meilleurs_livreurs;
	
	function nb_liv_liste (tete : T_pteur_liv) return integer is
		n : integer :=0;
		p : T_pteur_liv := tete;
	begin
		while p /= null loop
			n := n+1;
			p := p.suiv;
		end loop;
		return n;
	end nb_liv_liste;
	
	procedure retour_pause (tab : in out T_tab_liv; fait : out boolean) is
		nom_liv : T_mot;
		p : T_pteur_liv;
	begin
		put_line ("Quel est le nom du livreur revenant de pause ?");
		saisie_text (nom_liv);
		
		for sec in T_secteur'range loop
			p := tab (sec, en_pause);
			while p /= null loop
				change_liv_liste (nom_liv, tab(sec, en_pause), tab(sec, disponible), fait);
				exit when fait;
				p := p.suiv;
			end loop;
			exit when fait;
		end loop;
		
	end retour_pause;
	
		procedure depart_pause (tab : in out T_tab_liv; fait : out boolean) is
			nom_liv : T_mot;
			p : T_pteur_liv;
		begin
			put_line ("Quel est le nom du livreur partant en pause ?");
			saisie_text (nom_liv);
			
			for sec in T_secteur'range loop
				p := tab (sec, disponible);
				while p/= null loop
					change_liv_liste (nom_liv, tab(sec, disponible), tab(sec, en_pause), fait);
					exit when fait;
					p := p.suiv;
				end loop;
				exit when fait;
			end loop;
		end depart_pause;
		
	function liv_dispo (tab : T_tab_liv; secteur : T_secteur) return boolean is
		
	begin
		if tab(secteur, disponible) /= null then
			return true;
		else 
			return false;
		end if;
	end liv_dispo;
	
		procedure nouvelle_annee (tab : in T_tab_liv) is
			p : T_pteur_liv;
		begin
			for sec in T_secteur'range loop
				for sta in T_statut_liv'range loop
					p := tab(sec, sta);
					while p /= null loop
						p.nb_com_an := 0;
						p := p.suiv;
					end loop;
				end loop;
			end loop;
		end nouvelle_annee;
	
end gestion_livreur;
	

