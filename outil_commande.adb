with ada.text_io, ada.integer_text_io, ada.Integer_Text_IO, Ada.Numerics.Elementary_Functions;
use ada.text_io, ada.integer_text_io, ada.Integer_Text_IO;

PACKAGE BODY Outil_Commande IS

   PROCEDURE Affiche_Article(Article: IN T_Article) IS

   BEGIN
      FOR Article IN T_Article LOOP
         Put(T_Article'Image(Article));
         Put(",");
         END LOOP;
   END Affiche_Article;

     PROCEDURE Saisie_Text (S : OUT T_mot) IS

      K : Natural;
      Bonstring : Boolean; --Determine si le string est bon (true)

      BEGIN
         LOOP
            Bonstring := True;
            Get_Line (S,K);
            FOR I IN S'First..K LOOP --Verifie que le # est absent de la chaine saisie.
               IF S(I) = '#' THEN
                  Bonstring := False;
               END IF;
            END LOOP;
            EXIT WHEN Bonstring;
            Put_Line ("Votre saisie contient un # qui est un caractère interdit ! Recommencez.");
         END LOOP;
         FOR I IN K+1..S'Last LOOP
            S(I) := '#';
         END LOOP;
      END Saisie_Text;


   PROCEDURE Affiche_Text(S :IN T_Mot) IS

begin
	for i in S'range loop
		if S(i) = '#' then
			exit;
		end if;
		put(s(i));
	end loop;


END Affiche_Text;

	procedure saisie_nombre (n : out integer) is
		
	begin
		loop
		begin
			get (n);
			exit;
			exception
			when others => skip_line; put_line ("Mauvaise saisie, recommencer");
		end;
		end loop;
	end saisie_nombre;

      procedure affiche_nombre (n : in integer) is
	base : integer;
Begin

	if n< 0 then
		base := integer (Ada.Numerics.Elementary_Functions.Log(float(-n) +1.0, 10.0));
		base := base +1;
	else
		base := integer (Ada.Numerics.Elementary_Functions.Log(float(n) +1.0, 10.0));
	end if;

	put (n, base);
end affiche_nombre;


procedure getsecure  (i : out integer) is

begin
loop
	begin
	get (i);
	exit;
	exception
	when data_error => skip_line; put_line ("Erreur de saisie, recommencer");
	end;
end loop;
end getsecure;
   end outil_commande;
