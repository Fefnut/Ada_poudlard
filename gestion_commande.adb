WITH Ada.Text_IO, Ada.Integer_Text_IO, Gestion_Livreur, Outil_Commande, sauvegarde;
Use ada.Text_IO, ada.Integer_Text_IO, gestion_livreur, outil_commande, sauvegarde;

Package body gestion_commande is


Procedure Creation_Commande ( file_attente: IN OUT T_tab_att_com) is

 Procedure nouvelle_commande (com : out T_Pteur_com; secteur : out T_secteur) is
      choix_com, Id_Com, facture_tot, frais_port : integer;
      total : integer := 0;
      detail : T_Tab_DetailCom;
	nom_client : T_mot;
	Les_Prix : CONSTANT T_Tab_DetailCom := (10, 25, 75, 10, 58, 67);

   Begin

   Put ("Veuillez entrer le nom du client : ");
   Saisie_text (nom_client);

   Put ("Voici les secteurs disponibles : "); new_line;
      FOR I IN NE .. SO LOOP
         Put(T_Secteur'Pos(I) +1);
         Put (" : ");
         Put(T_Secteur'Image(I));
         New_Line;
      END LOOP;

      LOOP
         Put ("Veuillez saisir le chiffre du secteur de votre commande : ");
         saisie_nombre (choix_com);
         EXIT WHEN choix_com IN 1..6;
      END LOOP;
      secteur := T_Secteur'Val(choix_com -1);


      Put ("Veuillez entrer la composition de la commande : ");
      New_Line;
      FOR I IN detail'RANGE LOOP
         Put(T_article'Image(I));
         Put("( ");
         affiche_nombre(Les_Prix(I));
         Put(" euros");
         Put(" ) ");
         Put(" => ");
         saisie_nombre (detail(I));
      END LOOP;

	  Id_com := charge_integer ("Dernier_id_com.sav");
      Id_Com := Id_Com + 1;
      sauv_integer (Id_com, "Dernier_id_com.sav");
      Put ("L'identifiant de la commande est : ");
      affiche_nombre (Id_com);
      New_Line;
	  FOR I IN detail'RANGE LOOP
         Total := Total + Les_Prix(I) * detail(I);
      END LOOP;
      Put ("Le prix total de la commande sans les charges est : ");
      affiche_nombre(Total);
      Put (" Euros");
      New_Line;
      Put ("Le montant de votre facture s'eleve donc a : ");
      frais_port :=(Total * 6)/100;
      IF frais_port < 20 THEN
         Facture_tot := Total + frais_port;
      ELSE Facture_tot := Total + 20;
      END IF;
      affiche_nombre(Facture_tot);
      Put(" Euros");New_Line;
      new_line;

      com := New T_cell_com;
      com.Nom_c := nom_client;
      com.Id := id_com;
      com.detail_Com := detail;
      com.facture := facture_tot;
   end nouvelle_commande;

   com : t_pteur_com;
   secteur : T_secteur;
   Begin

   Nouvelle_commande (com, secteur);
   Enfile_commande (file_attente(secteur), com);

End Creation_Commande;
-- Je n'ai pas d�fini quel livreur prend la commande !

Procedure affiche_detail_com (t : T_tab_detailCom) is

begin
	for art in T'range loop
		put ("        " & T_Article'image(art) & " => ");
		affiche_nombre(t(art));
		new_line;
	end loop;
end affiche_detail_com;

Procedure Affiche_Cellule (cell_com : IN T_Cell_Com) is

Begin

Put ("        Nom du client: "); affiche_text (cell_com.Nom_C); new_line;
Put ("        Numero de la commande: "); affiche_nombre (cell_com.Id); new_line;
Put ("        Les articles: ");new_line; affiche_detail_com (cell_com.detail_com);
Put ("        Le montant de la facture: "); affiche_nombre (cell_com.Facture); new_line;
Put ("        Le nom du livreur: "); affiche_text (cell_com.Nom_Livreur); new_line;

End Affiche_Cellule;

Procedure affiche_list_com (tete : in T_pteur_com) is
	Procedure esc (tete : in T_pteur_com) is
	begin
		if tete /= null then
			affiche_cellule (tete.all);
			new_line;
			esc (tete.suiv);
		end if;
	end esc;

	begin

	if tete = null then
		put ("        VIDE"); new_line;
	else
		esc (tete);
	end if;
end affiche_list_com;

procedure choix_statut_com ( statut : out T_statut_com) is
	choix : integer;
	begin
	  Put ("Quel est le statut de la commande ?");
      New_Line;
      FOR I IN Attente .. Annulee LOOP
         affiche_nombre(T_statut_com'Pos(I) +1);
         Put (" : ");
         Put(T_Statut_com'Image(I));
         New_Line;
      END LOOP;
      New_Line;
      LOOP
         Put ("Veuillez saisir le chiffre correspondant : ");
         saisie_nombre (Choix);
         skip_Line;
         EXIT WHEN Choix IN 1..5;
      END LOOP;

      statut := T_statut_com'val(choix -1);
end choix_statut_com;

procedure choix_secteur (secteur : out T_secteur) is
	choix: integer;
begin
	  Put ("Quel est le secteur de la commande ?");
      New_Line;
      FOR I IN T_secteur'range LOOP
         affiche_nombre(T_secteur'Pos(I) +1);
         Put (" : ");
         Put(T_secteur'Image(I));
         New_Line;
      END LOOP;
      New_Line;
      LOOP
         Put ("Veuillez saisir le chiffre correspondant : ");
         saisie_nombre (Choix);
         skip_Line;
         EXIT WHEN Choix IN 1..6;
      END LOOP;
      secteur := T_secteur'val(choix-1);
end choix_secteur;

PROCEDURE Affiche_toute_Commande_filtre ( attente_com : T_tab_att_com; tab_prep_att_paiement, tab_archive : T_tab_list_com) is
      secteur : T_secteur;
      statut : T_statut_com;
   BEGIN
	  choix_statut_com (statut);
	  choix_secteur (secteur);

      case statut is
		when Attente => affiche_list_com (attente_com (secteur).tete);
		when Preparation | Attente_reglement => affiche_list_com (tab_prep_att_paiement (statut, secteur));
		When Reglee | Annulee => affiche_list_com (tab_archive (statut, secteur));
	  end case;
   end Affiche_toute_Commande_filtre;

   Procedure affiche_toute_commande (attente_com : T_tab_att_com; tab_prep_att_paiement, tab_archive : T_tab_list_com) is

   begin
   for secteur in T_secteur'range loop
		put_line (T_secteur'image(secteur));
		put_line ("    Attente");
		affiche_list_com (attente_com (secteur).tete);
		put_line ("    En preparation");
		affiche_list_com (tab_prep_att_paiement (Preparation, secteur));
		put_line ("    En attente de reglement");
		affiche_list_com (tab_prep_att_paiement (Attente_Reglement, secteur));
		put_line ("    Reglee");
		affiche_list_com (tab_archive (Reglee, secteur));
		put_line ("    Annulee");
		affiche_list_com (tab_archive (Annulee, secteur));
	end loop;
   end affiche_toute_commande;

   Procedure enfile_commande (file : in out t_file_com; pt : in t_pteur_com) is
   begin
      if file.tete = null then
         file.tete := new t_cell_com'(pt.nom_C, pt.id, pt.detail_com, pt.facture, pt.nom_livreur, null);
         file.fin := file.tete;
      else
         file.fin.suiv := new t_cell_com'(pt.nom_C, pt.id, pt.detail_com, pt.facture, pt.nom_livreur, null);
         file.fin := file.fin.suiv;
      end if;
   END Enfile_Commande;

   Procedure empile_commande (tete : in out T_pteur_com; pt : in t_pteur_com) is

	begin
		tete := new T_cell_com'(pt.Nom_C, pt.Id, pt.Detail_Com, pt.Facture, pt.Nom_Livreur, tete);
	end empile_commande;


      PROCEDURE Defiler_Commande(F: IN OUT T_File_Com; P : OUT T_Pteur_Com) IS

      BEGIN
         P:=F.Tete;
         F.Tete:=F.Tete.Suiv;

        END Defiler_Commande;


	function cherche_com (tete : T_pteur_com; Id_com : integer) return T_pteur_com is

		begin
			if tete = null then
				return null;
			elsif tete.Id = Id_com then
				return tete;
			else
				if tete.suiv = null then
					return null;
				else
					return cherche_com (tete.suiv, Id_com);
				end if;
			end if;
	end cherche_com;

	Procedure change_liste_com ( Id_com : in integer; tete_dep, tete_arr : in out T_pteur_com; erreur : out boolean) is
		aux : T_pteur_com;
	Begin
		if tete_dep = null then
			erreur := true;
		elsif tete_dep.Id = Id_com then
			aux := tete_arr;
			tete_arr := tete_dep;
			tete_dep := tete_dep.suiv;
			tete_arr.suiv := aux;
			erreur := false;
		else
			change_liste_com (Id_com, tete_dep.suiv, tete_arr, erreur);
		end if;
	end change_liste_com;

	function nb_com_list (tete : T_pteur_com) return integer is
		n : integer := 0;
		p : T_pteur_com := tete;
	begin
		while p/= null loop
			n := n+1;
			p := p.suiv;
		end loop;
		return n;
	end nb_com_list;


	function nb_com_annule (archive : T_tab_list_com) return integer is
		n : integer := 0;
	begin
		begin
			For sec in archive'range(2) loop
				n := n + nb_com_list(archive(Annulee, sec));
			end loop;
			exception
			When others => n := 0; Put_line ("Impossible de compter le nombre commande annule, etes vous sure que le bon tableau est utilis� ?");
		end;
	return n;
	end nb_com_annule;


	procedure changement_statut_commande (dep, arrive : in out T_tab_list_com; Id_com : in integer; erreur : out boolean; stat_dep, stat_arrive : in T_statut_com) is

	begin
		for zone in T_secteur'range loop
			change_liste_com ( Id_com, dep(stat_dep, zone), arrive(stat_arrive, zone), erreur);
			if not (erreur) then
				exit;
			end if;
		end loop;
	end changement_statut_commande;
	
	
	
	function commande_dans_liste (pt : T_pteur_com; Id_com : integer) return boolean is
		p : T_pteur_com := pt;
	begin
		while p /= null loop
			if p.Id = Id_com then
				return true;
			end if;
			p := p.suiv;
		end loop;
		return false;

	end commande_dans_liste;

	



	function donne_nom_liv (id_com : integer; tab : T_tab_list_com) return T_mot is
		p : T_pteur_com;
	begin
		for stat in tab'range (1) loop
			for sect in tab'range(2) loop
				p := tab(stat, sect);
				while p /= null loop
				if tab(stat, sect).id = id_com then
					return tab(stat, sect).Nom_livreur;
				end if;
				p := p.suiv;
				end loop;
			end loop;
		end loop;
		return ("##############################");
	end donne_nom_liv;


	procedure assigne_com (att_com : in out T_tab_att_com; tab_prep : in out T_tab_list_com; tab_liv : in out T_tab_liv; secteur : in T_secteur; Id_com : in integer; erreur : out boolean) is
		fait : boolean;
		nom_liv : T_mot;
		com : T_pteur_com:= cherche_com (att_com(secteur).tete, Id_com);
		procedure ajoute_com_liv (nom_liv : in T_mot; tete : in out T_pteur_liv ; com : in T_pteur_com) is
			p : T_pteur_liv := tete;
		begin
			while p /= null loop
				if p.nom = nom_liv then
					p.nb_com_an := p.nb_com_an +1;
					p.mont_com_tot := p.mont_com_tot + com.facture;
					exit;
				end if;
				p := p.suiv;
			end loop;
		end ajoute_com_liv;
	begin
		min_chiffre(tab_liv(secteur, Disponible), nom_liv);
		if com /= null then
			change_liv_liste (nom_liv, tab_liv(secteur, Disponible), tab_liv(secteur, en_commande), fait);
			if fait then
				com.Nom_livreur := nom_liv;
				ajoute_com_liv (nom_liv, tab_liv (secteur, en_commande), com);
				change_liste_com (Id_com, att_com(secteur).tete, tab_prep (Preparation, secteur), erreur);
				if erreur then
					put_line ("Erreur lors du changement de liste");
				end if;
			else
				Put_line ("Le livreur n'est pas trouv�, il faudra recommencer plus tard");
				erreur := true;
			end if;
		else
			Put_line ("L'assignation de commande n'a pas trouver la commande");
			erreur := true;
		end if;
	end assigne_com;

	FUNCTION Choix_Commande (tab_file : IN T_tab_att_com ; Secteur_Livreur_Disp : IN T_Secteur) RETURN T_Pteur_Com  is--prend la premiere commande du secteur choisi
	BEGIN
		RETURN tab_file(Secteur_Livreur_Disp).Tete;
	end choix_commande;
	
	
	procedure peut_assigner_commande (tab_file : T_tab_att_com; tab_liv : T_tab_liv; oui : out boolean; secteur : out T_secteur; id_com : out integer) is
		
	begin
		oui := false;
		for sec in T_secteur'range loop
			if liv_dispo (tab_liv, sec) then
				if tab_file(sec).tete /= null then
					id_com := tab_file(sec).tete.id;
					oui := true;
					secteur := sec;
					exit;
				end if;
			end if;
		end loop;
	end peut_assigner_commande;
	
	
	function cherche_com_tab (t : T_tab_list_com; Id_com : integer) return T_pteur_com is
		p : T_pteur_com;
	begin
		for sec in t'range(1) loop
			for stat in t'range(2) loop
				p := t (sec, stat);
				while p /= null loop
					if p.Id = Id_com then
						return p;
					end if;
					p := p.suiv;
				end loop;
			end loop;
		end loop;
		return null;
	end cherche_com_tab;


End gestion_commande;
