WITH Gestion_Livreur, Outil_Commande;
Use gestion_livreur, outil_commande;

Package gestion_commande is

   TYPE T_Statut_Com IS (Attente, Preparation, Attente_Reglement, Reglee, Annulee);

   TYPE T_Tab_DetailCom IS ARRAY (T_Article )OF Integer; --quantite de chaque produit


Type T_Cell_Com;
Type T_Pteur_Com is access T_Cell_Com;
Type T_Cell_Com is record
	Nom_C : T_mot;
	Id : integer;
	Detail_Com : T_Tab_DetailCom;
	Facture : integer;
	Nom_Livreur : T_mot := ("Pas de livreur assigne########");
	suiv : T_Pteur_Com;
END RECORD;

Type T_File_com is record -- Pour les files d'attentes
	Tete, Fin : T_Pteur_Com;
END RECORD;
Type T_tab_att_com is array (T_secteur) of T_file_com; -- Tableau de file d'attente de commande par secteur
Type T_tab_list_com is array (T_Statut_com range <>, T_Secteur range <>) of T_pteur_com; -- Tableau reutilise pour la liste des archives et pour les en_preparation et attente_paiment



TYPE T_TabCom IS ARRAY (T_Secteur, T_Statut_Com) OF T_Pteur_Com;

Procedure affiche_detail_com (t : T_tab_detailCom);--  Affiche le detail d'une commande
Procedure Affiche_Cellule (cell_com : IN T_Cell_Com); --   Affiche le contenu d'une cellule
Procedure affiche_list_com (tete : in T_pteur_com); -- 
PROCEDURE Affiche_toute_Commande_filtre ( attente_com : T_tab_att_com; tab_prep_att_paiement, tab_archive : T_tab_list_com); --  
Procedure affiche_toute_commande (attente_com : T_tab_att_com; tab_prep_att_paiement, tab_archive : T_tab_list_com); --  

procedure choix_statut_com ( statut : out T_statut_com); 
procedure choix_secteur (secteur : out T_secteur); 

Procedure Creation_Commande ( file_attente: IN OUT T_tab_att_com); --   Permet de creer une commande de num Id_com et de l'ajouter a la file d'attente.
Procedure change_liste_com ( Id_com : in integer; tete_dep, tete_arr : in out T_pteur_com; erreur : out boolean); --   Change la commande de num Id_com de la liste tete_dep vers la liste tete_arr

function cherche_com (tete : T_pteur_com; Id_com : integer) return T_pteur_com; --  Cherche la commande de num Id_com dans la liste tete, elle renvoie le pointeur vers la cellule. Renvoie null si ne trouve pas.
function cherche_com_tab (t : T_tab_list_com; Id_com : integer) return T_pteur_com;

procedure Defiler_Commande(F: in out T_File_Com; p : out T_pteur_com);
Procedure enfile_commande (file : in out t_file_com; pt : in t_pteur_com);
Procedure empile_commande (tete : in out T_pteur_com; pt : in t_pteur_com);

function nb_com_list (tete : T_pteur_com) return integer; --   compte le nombre de commande d'une liste de tete te
function nb_com_annule (archive : T_tab_list_com) return integer; --   nb de commandes annulees.


procedure changement_statut_commande (dep, arrive : in out T_tab_list_com; Id_com : in integer; erreur : out boolean; stat_dep, stat_arrive : in T_statut_com); --  permet de changer le statut d'une commande pre
procedure assigne_com (att_com : in out T_tab_att_com; tab_prep : in out T_tab_list_com; tab_liv : in out T_tab_liv; secteur : in T_secteur; Id_com : in integer; erreur : out boolean);


function donne_nom_liv (id_com : integer; tab : T_tab_list_com) return T_mot;
function commande_dans_liste (pt : T_pteur_com; Id_com : integer) return boolean; -- retourne vrai si la commande est dans la liste.

FUNCTION Choix_Commande (tab_file : IN T_tab_att_com ; Secteur_Livreur_Disp : IN T_Secteur) RETURN T_Pteur_Com;
procedure peut_assigner_commande (tab_file : T_tab_att_com; tab_liv : T_tab_liv; oui : out boolean; secteur : out T_secteur; id_com : out integer);

End gestion_commande;
