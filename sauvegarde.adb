with outil_commande, ada.text_io;
use outil_commande, ada.text_io;


package body sauvegarde is



	procedure sauv_integer (int : in integer; nom_fich : in string) is
		F : Fich_integer.File_type;
	begin
		begin
			open (F, out_file, nom_fich);
			Exception
			when others => Create (F, name=>nom_fich);
		end;
		write (F, int);
		close (f);
	end sauv_integer;

	function charge_integer (nom_fich : string) return integer is
		int : integer;
		F : fich_integer.File_type;
	begin
		begin
			open (F, in_file, nom_fich);
			read (f, int);
			close (f);
			exception
			when others => int := 0; Put_line ("Pas de numero de commande enregistre");
		end;
		return int;
	end charge_integer;

	function init_tab_nombre_liv (tab : in T_tab_liv) return T_tab_nombre_liv is
		t : T_tab_nombre_liv;
	begin
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				t (sec, stat) := nb_liv_liste (tab (sec, stat));
			end loop;
		end loop;
		return t;
	end init_tab_nombre_liv;

	procedure aff_tab_nombre_liv (t : T_tab_nombre_liv) is

	begin
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				affiche_nombre (t(sec, stat));
			end loop;
		end loop;
	end aff_tab_nombre_liv;

	procedure sauv_tab_nombre_liv (t : in T_tab_nombre_liv) is
		F : fich_tab_nombre_liv.File_type;
		nom_fich : constant string := "sauv_nombre_liv.sav";
	begin
		begin
			open(F, out_file, nom_fich);
			Exception
			When others => Create(F, name=>nom_fich);
		end;
	write(F, t);
	close(F);
	end sauv_tab_nombre_liv;

	function charge_tab_nombre_liv return T_tab_nombre_liv is
		t : T_tab_nombre_liv;
		F : fich_tab_nombre_liv.File_type;
		nom_fich : constant string := "sauv_nombre_liv.sav";
	begin
		open(F, in_file, nom_fich);
		read (f, t);
		close (f);
		return t;
	end charge_tab_nombre_liv;



	procedure aff_tab_list_liv (tab : in T_tab_list_liv) is

	begin
		for i in tab'range loop
			affiche_liv (tab(i));
		end loop;
	end aff_tab_list_liv;

	procedure cree_tab_liv (tete : T_pteur_liv; t : out T_tab_list_liv) is
		p : T_pteur_liv := tete;
	begin
		for i in t'range loop
			t(i) := p.all;
			p := p.suiv;
		end loop;
	end cree_tab_liv;

	function cree_list_liv (t : T_tab_list_liv) return T_pteur_liv is
		tete : T_pteur_liv := null;
	begin
		for i in t'range loop
			tete := new T_cell_liv'(t(i).nom,t(i).nb_com_an, t(i).mont_com_tot, tete);
		end loop;
		return tete;
	end cree_list_liv;

	procedure sauv_tab_liv (t : in T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv) is
		F : Fich_tab_liv.File_type;
		nom_fich : constant string := ("tab_liv" & T_secteur'image (secteur) & T_statut_liv'image (statut) & ".sav");
	begin
		begin
			open(F, out_file, nom_fich);
			Exception
			When others => Create(F, name=>nom_fich);
		end;
	write(F, t);
	close(F);
	end sauv_tab_liv;

	procedure charge_tab_liv (t : out T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv) is
		F : Fich_tab_liv.File_type;
		nom_fich : constant string := ("tab_liv" & T_secteur'image (secteur) & T_statut_liv'image (statut) & ".sav");
	Begin
		open(F, in_file, nom_fich);
		read (f, t);
		close (f);
	end charge_tab_liv;


	procedure sauv_tous_liv (t : in T_tab_liv) is
		nb_liv : T_tab_nombre_liv;
	begin
		nb_liv := init_tab_nombre_liv (t);
		sauv_tab_nombre_liv (nb_liv);
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				declare
					list : T_tab_list_liv (1..nb_liv(sec, stat));
				begin
					cree_tab_liv (t(sec, stat), list);
					sauv_tab_liv (list, sec, stat);
				end;
			end loop;
		end loop;

	end sauv_tous_liv;


	function charge_tous_liv return T_tab_liv is
		t : T_tab_liv;
		nb_liv : T_tab_nombre_liv;
	begin
		begin
		nb_liv := charge_tab_nombre_liv;
		for sec in T_secteur'range loop
			for stat in T_statut_liv'range loop
				declare
					list : T_tab_list_liv (1..nb_liv(sec, stat));
				begin
					charge_tab_liv (list, sec, stat);
					t(sec, stat) := cree_list_liv(list);
				end;
			end loop;
		end loop;
		exception
		When others => Put_line ("Probleme de chargement, livreurs non charges, peut être n'y a t'il pas de sauvegarde ...");
		end;
		return t;
	end charge_tous_liv;

	function init_tab_nombre_com (f_attente : in T_tab_att_com; tab_prep_att_paiement, tab_archive : in T_tab_list_com) return T_tab_nombre_com is
		t : T_tab_nombre_com;
	begin
		for sec in T_secteur'range loop
			t (sec, Attente) := nb_com_list (f_attente(sec).tete);
			t (sec, Preparation) := nb_com_list (tab_prep_att_paiement(Preparation, sec));
			t (sec, Attente_Reglement) := nb_com_list (tab_prep_att_paiement (Attente_reglement, sec));
			t (sec, Reglee) := nb_com_list (tab_archive (Reglee, sec));
			t (sec, Annulee) := nb_com_list (tab_archive (Annulee, sec));
		end loop;
		return t;
	end init_tab_nombre_com;

	procedure aff_tab_nombre_com (t : in T_tab_nombre_com) is

	begin
		for sec in t'range(1) loop
			for stat in t'range(2) loop
				affiche_nombre (t(sec, stat));
			end loop;
		end loop;
	end aff_tab_nombre_com;

	procedure sauv_tab_nombre_com (t  : in T_tab_nombre_com) is
		F : Fich_tab_nombre_com.File_type;
		nom_fich : string := "sauv_nombre_com.sav";
	begin
		begin
			open (F, out_file, nom_fich);
			exception
			when others => Create (F, name=>nom_fich);
		end;
		write (F, t);
		close (F);
	end sauv_tab_nombre_com;

	function charge_tab_nombre_com return T_tab_nombre_com is
		t : T_tab_nombre_com;
		F : fich_tab_nombre_com.File_type;
		nom_fich : constant string := "sauv_nombre_com.sav";
	begin
		open(F, in_file, nom_fich);
		read (f, t);
		close (f);
		return t;
	end charge_tab_nombre_com;





	procedure aff_tab_com (tab : in T_tab_com) is

	begin
		for i in tab'range loop
			affiche_cellule (tab(i));
			new_line;
		end loop;
	end aff_tab_com;


	procedure cree_tab_com (tete : T_pteur_com; t : in out T_tab_com) is
		p : T_pteur_com := tete;
	begin
		for i in t'range loop
			t(i) := p.all;
			p := p.suiv;
		end loop;
	end cree_tab_com;

	function cree_list_com (t : T_tab_com) return T_pteur_com is
		tete : T_pteur_com := null;
	begin
		for i in t'range loop
			tete := new T_cell_com'(t(i).nom_c, t(i).Id, t(i).detail_com, t(i).Facture, t(i).Nom_Livreur, tete);
		end loop;
		return tete;
	end cree_list_com;
	
	function cree_file_com (t : T_tab_com) return T_file_com is
		f : T_file_com;
		pt : T_pteur_com;
	begin
		pt := new T_cell_com;
		for i in t'range loop
			pt.all := t(i);
			enfile_commande (f, pt);
		end loop;
		return f;	
	end cree_file_com;
	
	procedure sauv_tab_com (t : in T_tab_com; secteur : T_secteur; statut : T_statut_com) is
		F : Fich_tab_com.File_type;
		nom_fich : constant string := ("tab_com" & T_secteur'image (secteur) & T_statut_com'image (statut) & ".sav");
	begin
		begin
			open (F, out_file, nom_fich);
			exception
			When others => Create(F, name=>Nom_fich);
		end;
		write (F, t);
		close(F);
	end sauv_tab_com;
	
	procedure charge_tab_com (t : out T_tab_com; secteur : T_secteur; statut : T_statut_com) is
		F: Fich_tab_com.File_type;
		nom_fich : constant string := ("tab_com" & T_secteur'image (secteur) & T_statut_com'image (statut) & ".sav");
	begin
		open (F, in_file, nom_fich);
		read (f, t);
		close (f);
	end charge_tab_com;
	
	
	procedure sauv_tous_com (att : in T_tab_att_com; tab_prep_att_paiement, tab_archive : in T_tab_list_com ) is
		nb_com : T_tab_nombre_com;
		procedure sauv_tab (t : T_tab_att_com ; stat : T_statut_com) is
			
		begin
			for sec in T_secteur'range loop
				declare
					list : T_tab_com (1..nb_com_list(t(sec).tete));
				begin
					cree_tab_com (t(sec).tete, list);
					sauv_tab_com (list, sec, stat);
				end;
			end loop;
		end sauv_tab;
		
		procedure sauv_tab (t : T_tab_list_com; stat : T_statut_com) is
			
		begin
			for sec in T_secteur'range loop
				declare
					list : T_tab_com (1..nb_com_list(t(stat, sec)));
				begin
					cree_tab_com (t(stat, sec), list);
					sauv_tab_com (list, sec, stat);
				end;
			end loop;
		end sauv_tab;
	begin
		nb_com := init_tab_nombre_com (att, tab_prep_att_paiement, tab_archive);
		sauv_tab_nombre_com (nb_com);
		
		for stat in T_statut_com'range loop
			case stat is
				when Attente => sauv_tab (att, stat);
				when Preparation | Attente_reglement => sauv_tab(tab_prep_att_paiement, stat);
				When Reglee | Annulee => sauv_tab (tab_archive, stat);
			end case;
		end loop;
				
	end sauv_tous_com;
	
	procedure charge_tous_com (att : out T_tab_att_com; tab_prep_att_paiement, tab_archive : out T_tab_list_com) is
		
		function charge_com (stat : T_statut_com; nb_com : in T_tab_nombre_com) return T_tab_att_com is
			t : T_tab_att_com;
		begin
			for sec in T_secteur'range loop
				declare
					list : T_tab_com (1..nb_com(sec, stat));
				begin
					charge_tab_com (list, sec, stat);
					t(sec) := cree_file_com (list);
				end;
			end loop;
			return t;
		end charge_com;
		
		procedure charge_com (stat : T_statut_com; nb_com : in T_tab_nombre_com; t : out T_tab_list_com) is
			
		begin
			for sec in T_secteur'range loop
				declare
					list : T_tab_com (1..nb_com(sec, stat));
				begin
					charge_tab_com (list, sec, stat);
					t(stat, sec) := cree_list_com (list);
				end;
			end loop;
		end charge_com;
		
		nb_com : T_tab_nombre_com;
		
	begin
		begin
		nb_com := charge_tab_nombre_com;
		for stat in T_statut_com'range loop
			case stat is
				when Attente => att := charge_com (stat, nb_com);
				when Preparation | Attente_reglement => charge_com (stat, nb_com, tab_prep_att_paiement);
				When Reglee | Annulee => charge_com (stat, nb_com, tab_archive);
			end case;
		end loop;
		exception
		when others => Put_line ("Probleme de chargement, commandes non charges, peut être n'y a t'il pas de sauvegarde ...");
		end;
	end charge_tous_com;
	

end sauvegarde;
