with gestion_livreur, gestion_commande, ada.sequential_io;
use gestion_livreur, gestion_commande;

package sauvegarde is

	
	
	package fich_integer is new ada.sequential_io(integer);
	use fich_integer;
	
	procedure sauv_integer (int : in integer; nom_fich : in string); --sauvegarde un entier dans un fichier nom_fich. Utile pour connaitre le nombre de commande.
	function charge_integer (nom_fich : string) return integer;
	
	Type T_tab_nombre_liv is array (T_secteur, T_statut_liv) of integer; -- Tableau qui contient le nombre de livreur dans chaque liste.
	package fich_tab_nombre_liv is new ada.sequential_io(T_tab_nombre_liv);
	use fich_tab_nombre_liv;
	
	function init_tab_nombre_liv (tab : in T_tab_liv) return T_tab_nombre_liv;
	procedure aff_tab_nombre_liv (t : T_tab_nombre_liv);
	procedure sauv_tab_nombre_liv (t : in T_tab_nombre_liv);
	function charge_tab_nombre_liv return T_tab_nombre_liv;
	
	Type T_tab_list_liv is array (integer range <>) of T_cell_liv;
	package fich_tab_liv is new ada.sequential_io(T_tab_list_liv);
	use fich_tab_liv;
	
	procedure aff_tab_list_liv (tab : in T_tab_list_liv); --affiche le tableau de livreur.
	procedure cree_tab_liv (tete : T_pteur_liv; t : out T_tab_list_liv); -- retourne un tableau de livreur à partir d'une liste de livreur.
	function cree_list_liv (t : T_tab_list_liv) return T_pteur_liv; --retourne list à partir du tableau.
	procedure sauv_tab_liv (t : in T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv);
	procedure charge_tab_liv (t : out T_tab_list_liv; secteur : T_secteur; statut : T_statut_liv);
	
	procedure sauv_tous_liv (t : in T_tab_liv); --sauvegarde tous les livreurs d'un coup, dans plusieurs fichiers.
	function charge_tous_liv return T_tab_liv;
	
	
	Type T_tab_nombre_com is array (T_secteur, T_statut_com) of integer;
	package fich_tab_nombre_com is new ada.sequential_io (T_tab_nombre_com);
	use fich_tab_nombre_com;
	
	function init_tab_nombre_com (f_attente : in T_tab_att_com; tab_prep_att_paiement, tab_archive : in T_tab_list_com) return T_tab_nombre_com; -- 
	procedure aff_tab_nombre_com (t : in T_tab_nombre_com);--  fonctionne mais difficile à lire
	procedure sauv_tab_nombre_com (t  : in T_tab_nombre_com);
	function charge_tab_nombre_com return T_tab_nombre_com;
	
	Type T_tab_com is array (integer range <>) of T_cell_com;
	package fich_tab_com is new ada.sequential_io(T_tab_com);
	use fich_tab_com;
	
	procedure aff_tab_com (tab : in T_tab_com); -- 
	procedure cree_tab_com (tete : T_pteur_com; t : in out T_tab_com);
	function cree_list_com (t : T_tab_com) return T_pteur_com;
	function cree_file_com (t : T_tab_com) return T_file_com; 
	procedure sauv_tab_com (t : in T_tab_com; secteur : T_secteur; statut : T_statut_com);
	procedure charge_tab_com (t : out T_tab_com; secteur : T_secteur; statut : T_statut_com);
	
	procedure sauv_tous_com (att : in T_tab_att_com; tab_prep_att_paiement, tab_archive : in T_tab_list_com ); 
	procedure charge_tous_com (att : out T_tab_att_com; tab_prep_att_paiement, tab_archive : out T_tab_list_com);

	
end sauvegarde;
